# Feeds directory

This is a temporary placeholder directory for hard coded XML podcast RSS feeds. 
For this to work, we will rely on https://raw.githack.com/ to serve the necessary
headers for our XML.
