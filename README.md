# LibriCast

A customizable RSS generator based on LibriVox content.

This tool will allow users to build custom Podcast RSS feeds with any combination
of Books and even Sections from Audiobooks available on [LibriVox](https://librivox.org).

## Road map

Right now, this project is in early development. The plan is to Cache an 
searchable list of Books and Sections from LibriVox. We will then Allow users to 
create a feed, and select which Books or Sections to include in that feed. Once 
published, they and anybody else can subscribe to the RSS feed from their 
favorite Podcast app.

## Why this project?

I wanted to compile and listen to the suggested 10 year reading plan found in 
Encyclopedia Brittanica's: The Great Books of the Western World. Having a feed I 
can subscribe to of the specific sections of audiobooks available at LibriVox 
will allow me to see what is missing, and how much time is required to accomplish
this great task. I hope to do it in much less than 10 years.

## Contributing

We'll supply more detail soon. For now, have a look at the 
[issue queue](https://gitlab.com/jcandan/libricast/boards) to see where things 
stand.

## Temporary placeholder

Take advantage of the open source nature of this project. Until this project 
is capable of producting RSS feeds via a user interface, should one wish to hard 
code their own XML RSS, they can submit a pull request to include it in the feeds
directory. See [web/feeds/README.md](https://gitlab.com/jcandan/libricast/tree/master/web/feeds)
for more info.
